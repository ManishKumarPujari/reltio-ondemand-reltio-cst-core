package com.reltio.file;

import au.com.bytecode.opencsv.CSVWriter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.reltio.cst.util.AWSS3Util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CodingErrorAction;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Mallikarjuna.Aakulati@reltio.com Created : May 28, 2021
 */
public class ReltioS3CSVFileWriter implements ReltioFileWriter, AutoCloseable {
    private static Logger logger = LoggerFactory.getLogger(ReltioS3CSVFileWriter.class.getName());

    private static final String lineEnd = System.lineSeparator();
    public static final int INITIAL_STRING_SIZE = 128;
    private ByteArrayOutputStream stream = new ByteArrayOutputStream();
    private final CSVWriter writer;
    private AmazonS3 s3Client;
    private String bucket;
    private String fileName;
    private String encoding="UTF-8";
    private final Set<String> crosswalkUnique = new HashSet<String>();
    CharsetEncoder encoder = Charset.forName("UTF-8").newEncoder();

    public ReltioS3CSVFileWriter(String bucket, String fileName, String awsKey, String awsSecretKey, String awsRegion) throws IOException {
        encoder.onMalformedInput(CodingErrorAction.REPLACE);
        encoder.onUnmappableCharacter(CodingErrorAction.REPLACE);

        this.s3Client=AWSS3Util.getS3Client(awsKey, awsSecretKey, awsRegion);
        stream.write(0xef);
        stream.write(0xbb);
        stream.write(0xbf);
        OutputStreamWriter streamWriter = new OutputStreamWriter(stream);
        writer =  new CSVWriter(streamWriter);
        this.bucket = bucket;
        this.fileName = fileName;
    }
    
    public ReltioS3CSVFileWriter(String bucket, String fileName, String awsKey, String awsSecretKey, String awsRegion, String encoding)
            throws IOException {

        encoder.onMalformedInput(CodingErrorAction.REPORT);
        encoder.onUnmappableCharacter(CodingErrorAction.REPORT);
        
        this.s3Client=AWSS3Util.getS3Client(awsKey, awsSecretKey, awsRegion);
        OutputStreamWriter streamWriter = new OutputStreamWriter(stream, encoder.charset());
        writer =  new CSVWriter(streamWriter);
        this.bucket = bucket;
        this.fileName = fileName;
        this.encoding=encoding;
        encoder = Charset.forName(encoding).newEncoder();
    }

    public void writeToFile(String[] line) {
    	if (line == null)
            return;
    	synchronized (writer) {
        writer.writeNext(line);
    	}
    }

    public void close() throws IOException {
    	writer.flush();
    	if (this.fileName.endsWith("_RejectedRecords.csv") && StringUtils.countMatches(stream.toString(), writer.DEFAULT_LINE_END)>1){
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType("text/html");
            metadata.setContentEncoding(encoding);
            metadata.setContentLength(stream.toByteArray().length);
            writer.close();
            s3Client.putObject(this.bucket, this.fileName, new ByteArrayInputStream(stream.toByteArray()), metadata);
        	}
    }

    @Override
    public void writeBulkToFile(List<String> lines) {
        // TODO Not implemented

    }

    @Override
    public void writeToFile(String line) {
        // TODO Not Implemented

    }

	@Override
	public void writeToFile(List<String[]> lines) {
        if (lines != null) {
            synchronized (writer) {
                for (String[] line : lines) {
                    writer.writeNext(line);
                }
            }
        }
    }
	
    public void writeToFile(List<String[]> lines, boolean unique)
            throws IOException {
        if (unique) {
            if (lines != null) {
                synchronized (writer) {
                    for (String[] line : lines) {
                        if (crosswalkUnique.add(line[0] + line[1])) {
                            writer.writeNext(line);
                        }
                    }
                }
            }

        } else {
            writeToFile(lines);
        }

    }
}
