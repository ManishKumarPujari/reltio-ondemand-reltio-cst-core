package com.reltio.file;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;
import com.reltio.cst.util.AWSS3Util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Mallikarjuna.Aakulati@reltio.com Created : May 28, 2021
 */
public class ReltioS3FlatFileWriter implements ReltioFileWriter, AutoCloseable {
	private static Logger logger = LoggerFactory.getLogger(ReltioS3FlatFileWriter.class.getName());

	public static final int INITIAL_STRING_SIZE = 128;
	private static final String lineEnd = System.lineSeparator();

	private String separator = "|";

	private CharsetEncoder encoder = Charset.forName("UTF-8").newEncoder();

	private final FileOutputStream stream;
	private final OutputStreamWriter streamWriter;
	private final AmazonS3 s3Client;
	private String bucket;
	private String fileName;
	private String encoding="UTF-8";
	private String localFileName;
	TransferManager tm = null;

	/**
	 * By default UTF-8 encoding used and separator will be pipe
	 *
	 * @param fileName
	 * @throws IOException
	 */
	public ReltioS3FlatFileWriter(String bucket, String fileName, String awsKey, String awsSecretKey, String awsRegion) throws IOException {
		encoder.onMalformedInput(CodingErrorAction.REPORT);
		encoder.onUnmappableCharacter(CodingErrorAction.REPORT);
		String[] strArray = fileName.split("/");  
		this.localFileName = strArray[strArray.length -1];
		stream = new FileOutputStream (localFileName);
		streamWriter = new OutputStreamWriter(stream);

		this.s3Client=AWSS3Util.getS3Client(awsKey, awsSecretKey, awsRegion);
		this.bucket = bucket;
		this.fileName = fileName;

	}

	public ReltioS3FlatFileWriter(String bucket, String fileName, String awsKey, String awsSecretKey, String awsRegion, String encoding) throws IOException {
		encoder.onMalformedInput(CodingErrorAction.REPORT);
		encoder.onUnmappableCharacter(CodingErrorAction.REPORT);

		String[] strArray = fileName.split("/");  
		String localFileName = strArray[strArray.length -1]; 
		stream = new FileOutputStream (localFileName);
		streamWriter = new OutputStreamWriter(stream);

		this.s3Client=AWSS3Util.getS3Client(awsKey, awsSecretKey, awsRegion);
		this.bucket = bucket;
		this.fileName = fileName;
		this.encoding=encoding;
		encoder = Charset.forName(encoding).newEncoder();
	}

	public ReltioS3FlatFileWriter(String bucket, String fileName, String awsKey, String awsSecretKey, String awsRegion, String encoding, String separator) throws IOException {
		encoder.onMalformedInput(CodingErrorAction.REPORT);
		encoder.onUnmappableCharacter(CodingErrorAction.REPORT);

		String[] strArray = fileName.split("/");  
		String localFileName = strArray[strArray.length -1]; 
		stream = new FileOutputStream (localFileName);
		streamWriter = new OutputStreamWriter(stream);

		this.s3Client=AWSS3Util.getS3Client(awsKey, awsSecretKey, awsRegion);
		this.bucket = bucket;
		this.fileName = fileName;
		this.separator = separator;
		this.encoding=encoding;
		encoder = Charset.forName(encoding).newEncoder();
	}

	public void writeToFile(List<String[]> lines) throws IOException {
		if (lines != null) {
			synchronized (streamWriter) {
				for (String[] line : lines) {
					writeDataToFile(line);
				}
				streamWriter.flush();
			}
		}
	}

	public void close() throws IOException {
		streamWriter.flush();
		if (!this.fileName.endsWith("_RejectedRecords.txt") && !stream.toString().isEmpty()){
			try {
				ObjectMetadata metadata = new ObjectMetadata();
				metadata.setContentType("text/html");
				metadata.setContentEncoding(encoding);
				long start = System.currentTimeMillis();
				// metadata.setContentLength(stream.toByteArray().length);
				streamWriter.close();
				File localFile = new File(this.localFileName);
				metadata.setContentLength(localFile.length());
				ProgressListener progressListener = progressEvent -> System.out.println(
						  "Transferred bytes: " + progressEvent.getBytesTransferred());
				tm = TransferManagerBuilder.standard()
						.withS3Client(s3Client)
						.withMultipartUploadThreshold((long) (50* 1024 * 1025))
						.build();
				PutObjectRequest putObjectRequest = new PutObjectRequest(this.bucket, this.fileName, new FileInputStream(localFile), metadata);
				putObjectRequest.setGeneralProgressListener(progressListener);

				Upload upload = tm.upload(putObjectRequest);
				upload.waitForCompletion();
				long end = System.currentTimeMillis();
				long duration = (end - start)/1000;

				// Log status
				System.out.println("Successul upload in S3 multipart. Duration = " + duration);
				// s3Client.putObject(this.bucket, this.fileName, new FileInputStream(localFile), metadata);
				localFile.delete();

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (s3Client != null)
					s3Client.shutdown();
				if (tm != null)
					tm.shutdownNow();
			}
		}
		if (StringUtils.countMatches(stream.toString(), lineEnd)>1 && this.fileName.endsWith("_RejectedRecords.txt")){
			try {
				ObjectMetadata metadata = new ObjectMetadata();

				metadata.setContentType("text/html");
				metadata.setContentEncoding(encoding);
				//metadata.setContentLength(stream.toByteArray().length);
				streamWriter.close();
				long start = System.currentTimeMillis();
				File localFile = new File(this.localFileName);
				
				metadata.setContentLength(localFile.length());
				
				ProgressListener progressListener = progressEvent -> System.out.println(
						  "Transferred bytes: " + progressEvent.getBytesTransferred());
				tm = TransferManagerBuilder.standard()
						.withS3Client(s3Client)
						.withMultipartUploadThreshold((long) (50* 1024 * 1025))
						.build();
				PutObjectRequest putObjectRequest = new PutObjectRequest(this.bucket, this.fileName, new FileInputStream(localFile), metadata);
				putObjectRequest.setGeneralProgressListener(progressListener);

				Upload upload = tm.upload(putObjectRequest);
				upload.waitForCompletion();
				long end = System.currentTimeMillis();
				long duration = (end - start)/1000;

				// Log status
				System.out.println("Successul upload in S3 multipart. Duration = " + duration);
				// s3Client.putObject(this.bucket, this.fileName, new FileInputStream(localFile), metadata);
				localFile.delete();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (s3Client != null)
					s3Client.shutdown();
				if (tm != null)
					tm.shutdownNow();
			}

		}
	}

	/**
	 * Writes the next line to the file.
	 *
	 * @param nextLine a string array with each comma-separated element as a separate
	 *                 entry.
	 * @throws IOException
	 */
	public void writeToFile(String[] nextLine) throws IOException {
		synchronized (streamWriter) {
			writeDataToFile(nextLine);
			streamWriter.flush();
		}
	}

	private void writeDataToFile(String[] nextLine) throws IOException {
		if (nextLine == null)
			return;
		StringBuilder sb = new StringBuilder(INITIAL_STRING_SIZE);
		for (int i = 0; i < nextLine.length; i++) {

			if (i != 0) {
				sb.append(separator);
			}

			String nextElement = nextLine[i];
			if (nextElement == null)
				continue;
			sb.append(nextElement);
		}

		sb.append(lineEnd);
		streamWriter.write(sb.toString());
	}

	@Override
	public void writeBulkToFile(List<String> lines) throws IOException {
		if (lines != null && !lines.isEmpty()) {
			synchronized (streamWriter) {
				for (String line : lines) {
					streamWriter.write(line + lineEnd);
				}
				streamWriter.flush();
			}
		}
	}

	@Override
	public void writeToFile(String line) throws IOException {
		if (line != null) {
			synchronized (streamWriter) {
				streamWriter.write(line + lineEnd);
			}
			streamWriter.flush();
		}
	}

	/**
	 * @param separator the separator to set
	 */
	public void setSeparator(String separator) {
		this.separator = separator;
	}
}
