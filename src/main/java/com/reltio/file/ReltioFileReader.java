package com.reltio.file;

import java.io.IOException;

/**
 * This is the interface for reading any Flat/CSV File
 */
public interface ReltioFileReader extends AutoCloseable {

    /**
     * Reads next line for each call.
     *
     * @return
     * @throws IOException
     */
    public String[] readLine() throws IOException;

    /**
     * Closes the File stream opened initially
     *
     * @throws IOException
     */
    public void close() throws IOException;
}
