package com.reltio.cst.domain;

/**
 * 
 * List of HTTP methods supported
 * 
 *
 */
public enum HttpMethod {

	GET, PUT, POST, DELETE;

}
