package com.reltio.cst.service.impl;

import com.reltio.cst.domain.HttpMethod;
import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.service.RestAPIService;
import com.reltio.cst.util.GenericUtilityService;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * This Rest API service impl class using the direct Java API for Http Calls
 */
public class SimpleRestAPIServiceImpl implements RestAPIService {
    public static final Integer DEFAULT_TIMEOUT_IN_MINUTES = 5;
    private static Logger log = LoggerFactory.getLogger(SimpleRestAPIServiceImpl.class.getName());
    private static RequestsLogger logger = new RequestsLogger();

    static {
        try {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};
            // Install the all-trusting trust manager
            final SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = (hostname, session) -> true;

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private final int connectTimeout;

    public SimpleRestAPIServiceImpl(int timeoutInMinutes) {
        connectTimeout = (int) TimeUnit.MINUTES.toMillis(timeoutInMinutes);
    }

    public SimpleRestAPIServiceImpl() {
        connectTimeout = (int) TimeUnit.MINUTES.toMillis(DEFAULT_TIMEOUT_IN_MINUTES);
    }

    public static void setupRequestsLogger(String fileName) throws IOException {
        logger = new RequestsLogger(fileName);
    }

    public static void shutdownRequestsLogger() {
        logger.close();
        logger = new RequestsLogger();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.reltio.cst.service.RestAPIService#get(java.lang.String,
     * java.util.Map)
     */
    @Override
    public String get(String requestUrl, Map<String, String> requestHeaders)
            throws APICallFailureException, GenericException {

        return doExecute(requestUrl, requestHeaders, null, HttpMethod.GET);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.reltio.cst.service.RestAPIService#post(java.lang.String,
     * java.util.Map, java.lang.String)
     */
    @Override
    public String post(String requestUrl, Map<String, String> requestHeaders, String requestBody) throws APICallFailureException, GenericException {
        return doExecute(requestUrl, requestHeaders, requestBody, HttpMethod.POST);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.reltio.cst.service.RestAPIService#put(java.lang.String,
     * java.util.Map, java.lang.String)
     */
    @Override
    public String put(String requestUrl, Map<String, String> requestHeaders,
                      String requestBody) throws APICallFailureException,
            GenericException {

        return doExecute(requestUrl, requestHeaders, requestBody, HttpMethod.PUT);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.reltio.cst.service.RestAPIService#delete(java.lang.String,
     * java.util.Map, java.lang.String)
     */
    @Override
    public String delete(String requestUrl, Map<String, String> requestHeaders,
                         String requestBody) throws APICallFailureException,
            GenericException {
        return doExecute(requestUrl, requestHeaders, requestBody, HttpMethod.DELETE);
    }

    /**
     * This is common method to do all the different http method API calls
     *
     * @param requestUrl
     * @param requestHeaders
     * @param requestBody
     * @param requestMethod
     * @return
     * @throws GenericException
     */
    /*
     * (non-Javadoc)
     *
     * @see com.reltio.cst.service.RestAPIService#doExecute(java.lang.String,
     * java.util.Map, java.lang.String, com.reltio.cst.domain.HttpMethod)
     */
    @Override
    public String doExecute(String requestUrl,
                            Map<String, String> requestHeaders, String requestBody,
                            HttpMethod requestMethod) throws APICallFailureException, GenericException {
        long startTime = System.currentTimeMillis();
        try {

            URL obj = new URL(encodeURL(requestUrl));
            log.debug("Final Url " + obj.toString());
            HttpURLConnection connection = (HttpURLConnection) obj.openConnection();

            connection.setConnectTimeout(connectTimeout);
            connection.setReadTimeout(connectTimeout);

            connection.setRequestMethod(requestMethod.name());
            // Add all the header parameters to the URL
            if (requestHeaders != null) {
                for (Map.Entry<String, String> entry : requestHeaders.entrySet()) {
                    connection.setRequestProperty(entry.getKey(), entry.getValue());
                }
            }

            /*
             * HttpURLConntion fails because HttpURLConnection strips Content-Length header on Post
             */
            if (requestBody != null && requestBody.length() == 0 && !requestMethod.name().toUpperCase().equals("GET")) {
                connection.setFixedLengthStreamingMode(requestBody.length());
                connection.setDoOutput(true);
            }

            if (StringUtils.isNotEmpty(requestBody)) {
                connection.setDoOutput(true);
                try (OutputStream out = connection.getOutputStream()) {
                    IOUtils.write(requestBody, out, StandardCharsets.UTF_8);
                }
            }


            logger.logRequest(startTime, connection, requestHeaders, requestBody);

            // Verify the Response
            int responseCode = connection.getResponseCode();

            if (responseCode >= 400) {
                try (InputStream errorStream = connection.getErrorStream()) {
                    String errorResponse = IOUtils.toString(errorStream, StandardCharsets.UTF_8);
                    if (responseCode != 401) {
                        log.error("REST Api call failed... Response URL: "
                                + requestUrl + " | Error Code: " + responseCode
                                + " | Error Response: "
                                + connection.getResponseMessage()
                                + " |||| Error Message body: " + errorResponse);
                    }
                    logger.logResponse(startTime, responseCode, errorResponse);
                    throw new APICallFailureException(responseCode, errorResponse);
                }
            } else {
                try (InputStream inputStream = connection.getInputStream()) {
                    String response = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
                    // if the API call completed successfully
                    logger.logResponse(startTime, responseCode, response);
                    return response;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new GenericException(e.getLocalizedMessage());
        } catch (APICallFailureException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new GenericException(e.getLocalizedMessage());
        }
    }

    public String encodeURL(String url) throws GenericException {
        URL encodeUrl;
        String newUrlStr = url;
        try {
            encodeUrl = new URL(url);
            String query = encodeUrl.getQuery();
            if (!GenericUtilityService.checkNullOrEmpty(query)) {

                String encodedQuery = "";
                String[] queryParams = query.split("&");
                for (String param : queryParams) {

                    int index = param.indexOf("=");
                    if (!encodedQuery.isEmpty()) {
                        encodedQuery += "&";
                    }
                    String singleParam;
                    if (param.contains("%")) {
                        singleParam = param.substring(index + 1);
                    } else {
                        singleParam = URLEncoder.encode(param.substring(index + 1), StandardCharsets.UTF_8.name());
                    }
                    encodedQuery += param.substring(0, index)
                            + "="
                            + singleParam;
                }

                newUrlStr = encodeUrl.getProtocol() + "://"
                        + encodeUrl.getAuthority() + encodeUrl.getPath() + "?"
                        + encodedQuery;

            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new GenericException(
                    "Invalid URL... Verify the URL and provide the correct one...");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return newUrlStr;
    }
}

class RequestsLogger implements AutoCloseable {

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss.SSS");
    private final OutputStream os;

    public RequestsLogger() {
        os = null;
    }

    public RequestsLogger(String fileName) throws IOException {
        if (fileName != null) {
            os = new BufferedOutputStream(new FileOutputStream(StringEscapeUtils.escapeJava(fileName), true), 65536);
            StringBuilder sb = logStart(System.currentTimeMillis());
            sb.append(" Starting the Dataload..." + System.lineSeparator());
            write(sb.toString());
        } else {
            os = null;
        }
    }

    public void logRequest(long startTime, HttpURLConnection connection, Map<String, String> requestHeaders, String body) throws IOException {
        if (os == null) {
            return;
        }
        StringBuilder sb = logStart(startTime);

        sb
                .append(" (sent) ")
                .append(connection.getRequestMethod())
                .append(' ')
                .append(connection.getURL().toExternalForm());
        if (requestHeaders != null) {
            sb.append(' ')
                    .append(requestHeaders);
        }
        if (body != null) {
            sb.append(' ')
                    .append(body);
        }
        sb.append(System.lineSeparator());
        write(sb.toString());
    }

    public void logResponse(long startTime, int code, String response) throws IOException {
        if (os == null) {
            return;
        }
        StringBuilder sb = logStart(startTime);
        long delta = System.currentTimeMillis() - startTime;
        sb
                .append(" (received in ")
                .append(delta)
                .append(") ")
                .append(code)
                .append(' ')
                .append(response)
                .append(System.lineSeparator());
        write(sb.toString());
    }

    private StringBuilder logStart(long startTime) {
        StringBuilder sb = new StringBuilder();

        sb.append(dateFormat.format(new Date(startTime)))
                .append(' ')
                .append(Thread.currentThread().getName());
        return sb;
    }

    private synchronized void write(String line) throws IOException {
        os.write(line.getBytes(StandardCharsets.UTF_8));
    }

    @Override
    public void close() {
        if (os == null) {
            return;
        }
        try {
            StringBuilder sb = logStart(System.currentTimeMillis());
            sb.append(" Dataload finished.").append(System.lineSeparator());
            write(sb.toString());
            os.flush();
            os.close();
        } catch (Exception ignored) {
        }
    }
}