package com.reltio.cst.util;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class UtilTest {

    @Test
    public void getProperties() throws Exception {
        File resourcesDirectory = new File("src/test/resources/config.properties");
        System.out.println(resourcesDirectory.getAbsolutePath());

        Properties properties = Util.getProperties(resourcesDirectory.getAbsolutePath(), "PASSWORD", "TEST");
        Assert.assertEquals("reltio", properties.getProperty("PASSWORD"));
    }

    @Test
    public void listMissingProperties() throws Exception {

        File resourcesDirectory = new File("src/test/resources/config.properties");
//      properties.load(new InputStreamReader(new FileInputStream(new File(resourcesDirectory.getAbsolutePath()))));
        Properties properties = Util.getProperties(resourcesDirectory.getAbsolutePath(), "PASSWORD");
        List<String> requiredProperties = new ArrayList<>();
        requiredProperties.add("PASSWORD");
        requiredProperties.add("USERNAME");
        List<String> missingKeys = Util.listMissingProperties(properties, requiredProperties);
        Assert.assertEquals(1, missingKeys.size());

    }
}