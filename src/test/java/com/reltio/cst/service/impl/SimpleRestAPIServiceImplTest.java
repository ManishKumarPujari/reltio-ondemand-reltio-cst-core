package com.reltio.cst.service.impl;

import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.service.RestAPIService;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;


public class SimpleRestAPIServiceImplTest {

    private RestAPIService restAPIService = new SimpleRestAPIServiceImpl();

    @Test(expected = com.reltio.cst.exception.handler.APICallFailureException.class)
    public void testGet_APIcallFailure() throws APICallFailureException,
            GenericException {
        Map<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Authorization", "Basic cmVsdGlvX3VpOm1ha2l0YQ==");

        restAPIService.get("https://auth.reltio.com/oauth/token?username=sample&password=test&grant_type=password",
                        requestHeaders);
    }

    @Test(expected = com.reltio.cst.exception.handler.GenericException.class)
    public void testGet_InvalidURL() throws APICallFailureException,
            GenericException {
        Map<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Authorization", "Basic cmVsdGlvX3VpOm1ha2l0YQ==");

        restAPIService
                .get("https://auth222.reltio.com/oauth/token?username=test&password=test&grant_type=password",
                        requestHeaders);

    }

    @Test(expected = com.reltio.cst.exception.handler.GenericException.class)
    public void testGet_NullURL() throws APICallFailureException,
            GenericException {
        Map<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Authorization", "Basic cmVsdGlvX3VpOm1ha2l0YQ==");

        restAPIService.get(null, requestHeaders);

    }

    @Test
    public void testGet_Success() throws APICallFailureException,
            GenericException {
        Map<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Authorization", "Basic cmVsdGlvX3VpOm1ha2l0YQ==");

        String response = restAPIService
                .get("https://auth.reltio.com/oauth/token?username=" + TokenGeneratorServiceImplTest.username + "&password=" + TokenGeneratorServiceImplTest.password + "&grant_type=password",
                        requestHeaders);
        System.out.println(response);
        Assert.assertNotNull(response);
    }

    @Test(expected = com.reltio.cst.exception.handler.APICallFailureException.class)
    public void testPost_APIcallFailure() throws APICallFailureException,
            GenericException {
        Map<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Authorization", "Basic cmVsdGlvX3VpOm1ha2l0YQ==");

        restAPIService
                .post("https://auth.reltio.com/oauth/token?username=sample&password=test&grant_type=password",
                        requestHeaders, null);

    }

    @Test(expected = com.reltio.cst.exception.handler.GenericException.class)
    public void testPost_InvalidURL() throws APICallFailureException,
            GenericException {
        Map<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Authorization", "Basic cmVsdGlvX3VpOm1ha2l0YQ==");

        restAPIService
                .post("https://auth222.reltio.com/oauth/token?username=test&password=test&grant_type=password",
                        requestHeaders, null);

    }

    @Test(expected = com.reltio.cst.exception.handler.GenericException.class)
    public void testPost_NullURL() throws APICallFailureException,
            GenericException {
        Map<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Authorization", "Basic cmVsdGlvX3VpOm1ha2l0YQ==");

        restAPIService.post(null, requestHeaders, null);

    }

    @Test
    public void testPost_Success() throws APICallFailureException,
            GenericException {
        Map<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Authorization", "Basic cmVsdGlvX3VpOm1ha2l0YQ==");

        String response = restAPIService
                .post("https://auth.reltio.com/oauth/token?username=" + TokenGeneratorServiceImplTest.username + "&password=" + TokenGeneratorServiceImplTest.password + "&grant_type=password",
                        requestHeaders, null);
        System.out.println(response);
        Assert.assertNotNull(response);
    }

    @Test
    public void testDelete_Success() throws APICallFailureException,
            GenericException {
        Map<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Authorization", "Basic cmVsdGlvX3VpOm1ha2l0YQ==");

        String response = restAPIService
                .delete("https://auth.reltio.com/oauth/token?username=" + TokenGeneratorServiceImplTest.username + "&password=" + TokenGeneratorServiceImplTest.password + "&grant_type=password",
                        requestHeaders, null);
        System.out.println(response);
        Assert.assertNotNull(response);
    }

    @Test
    public void encodeURL() {

        String crosswalkValue = "https://sndbx.reltio.com/reltio/api/aO3zrh5wJaqmfGE/entities/qbbNfrb/attributes/FirstName/1NXDTJgRF?crosswalkValue=111%23";
//        URL encodeUrl = new URL(crosswalkValue);
//        System.out.println(encodeUrl.toString());
        String encodedUrl = null;
        SimpleRestAPIServiceImpl simpleRestAPIService = new SimpleRestAPIServiceImpl();
        try {
            encodedUrl = simpleRestAPIService.encodeURL(crosswalkValue);
        } catch (GenericException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(encodedUrl, crosswalkValue);
    }
}
