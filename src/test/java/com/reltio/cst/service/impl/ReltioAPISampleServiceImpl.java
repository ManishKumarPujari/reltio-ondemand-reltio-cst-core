package com.reltio.cst.service.impl;

import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.service.TokenGeneratorService;

/**
 * The sample Java class for using the Reltio API service implementation
 */
public class ReltioAPISampleServiceImpl {


    public static void main(String[] args) {

        //Step1: Create Reltio Token Generator Service
        TokenGeneratorService tokenGeneratorService = null;

        try {
            tokenGeneratorService = new TokenGeneratorServiceImpl(
                    TokenGeneratorServiceImplTest.username,
                    TokenGeneratorServiceImplTest.password,
                    "https://auth.reltio.com/oauth/token");
        } catch (APICallFailureException e) {
            //This Exception is thrown when username/password invalid. Or if Auth server throws any exception
            //Get the Http error code & error message sent by Reltio API as below
            System.out.println("Error Code: " + e.getErrorCode() + " >>>> Error Message: " + e.getErrorResponse());

        } catch (GenericException e) {
            //This exception thrown when unexpected error happens like "Auth Server is down"
            //Get the exception message as below
            System.out.println(e.getExceptionMessage());
        }

        //If any one exception happens then abort the process as without token, we can't do any Reltio API call.

        //Step2: Create Reltio API service. Give the tokenGeneratorService as input to the constructor
        ReltioAPIService reltioAPIService = new SimpleReltioAPIServiceImpl(tokenGeneratorService);

        //Step3: Now we can do any Reltio API call & n number of times using reltioAPIService and no need to worry about the token generation and retry process
        String response = null;
        try {
            response = reltioAPIService.get("http://localhost:9080/api/user/");

        } catch (GenericException e) {
            //This exception thrown when unexpected error happens like "Reltio Server is down/invalid input details"
            //Get the exception message as below
            System.out.println(e.getExceptionMessage());
        } catch (ReltioAPICallFailureException e) {
            //This Exception is thrown when Reltio API throws any exception
            //Get the Http error code & error message sent by Reltio API as below
            System.out.println("Error Code: " + e.getErrorCode() + " >>>> Error Message: " + e.getErrorResponse());

        }
        System.out.println(response);

        //The same way we can do all other API calls
    }

}
