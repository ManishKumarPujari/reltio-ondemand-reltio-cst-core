package com.reltio.file;

public class SimpleFileReaderTest {

	public static void main(String[] args) throws Exception {
		
		//Step 1: Create Object reference for ReltioFileReader Interface
		ReltioFileReader reltioFileReader;
		 
		//Step 2: Create a object based on the File Type.
			//if CSV then use below option
				//Step 2.1: Only with File Name using ReltioCSVFileReader
				reltioFileReader = new ReltioCSVFileReader("<FileName>");
				
				//With File Name & Decoding technique like UTF-8 or any other
				reltioFileReader = new ReltioCSVFileReader("<FileName>", "UTF-8");
			
			//ELSE if FLAT FILE then use below option
				//Step 2.1: Only with File Name. Default Delimiter is pipe
				reltioFileReader = new ReltioFlatFileReader("<FileName>");
				
				//With File Name & Separator like pipe, dollar
				reltioFileReader = new ReltioFlatFileReader("<FileName>","||");
				
				//With File Name, Separator & Decoding
				reltioFileReader = new ReltioFlatFileReader("<FileName>","||", "UTF-8");
		
		//Step 3: Read data from the File.
		//It always returns the parsed data. So no need to any parsing of data.
		String[] lineValues = reltioFileReader.readLine();
		
		//Step 4: If lineValues is null then it means reached the End of File.
		while((lineValues = reltioFileReader.readLine()) != null) { 
			System.out.println(lineValues);
			//Do required opertaion
		}
		
		
		//Step 5: Close the stream
		reltioFileReader.close();
		
	}

}
